package de.hska.spring.enterprise.domain;

import org.springframework.util.Assert;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Order extends AbstractModel {

    private List<Item> items = new ArrayList<>();

    private BigDecimal discount;

    private Customer customer;

    public Order(Customer customer, List<Item> items) {
        Assert.notNull(customer);
        Assert.notEmpty(items);

        this.customer = customer;
        this.items = items;
    }

    protected Order() {
        // hide default constructor
    }

    @Override
    public String toString() {
        return "Order{" +
                "items=" + items +
                ", discount=" + discount +
                ", customer=" + customer +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Order)) return false;
        if (!super.equals(o)) return false;

        Order order = (Order) o;

        if (!customer.equals(order.customer)) return false;
        if (!discount.equals(order.discount)) return false;
        if (!items.equals(order.items)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + items.hashCode();
        result = 31 * result + discount.hashCode();
        result = 31 * result + customer.hashCode();
        return result;
    }

    public List<Item> getItems() {
        return items;
    }

    public void addItem(Item item) {
        Assert.notNull(item);
        this.items.add(item);
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
}

